#pragma once

SQFUNC(sq_bass_stream_create_file);
SQFUNC(sq_bass_stream_create_url);
SQFUNC(sq_bass_stream_free);
SQFUNC(sq_bass_get_volume);
SQFUNC(sq_bass_set_volume);
SQFUNC(sq_bass_channel_stop);
SQFUNC(sq_bass_channel_play);
SQFUNC(sq_bass_channel_pause);
SQFUNC(sq_bass_channel_get_tags);