#include "PCH.h"

HSQAPI g_pApi = NULL;

void SqRegisterFunction(HSQUIRRELVM vm, const char *name, SQFUNCTION func)
{
	g_pApi->pushroottable(vm);
	g_pApi->pushstring(vm, name, -1);
	g_pApi->newclosure(vm, func, 0);
	g_pApi->newslot(vm, -3, SQFalse);
	g_pApi->pop(vm, 1);
}

void SqRegisterValue(HSQUIRRELVM vm, const char *name, int value)
{
	g_pApi->pushconsttable(vm);
	g_pApi->pushstring(vm, name, -1);
	g_pApi->pushinteger(vm, value);
	g_pApi->newslot(vm, -3, SQFalse);
	g_pApi->pop(vm, 1);
}

void SqRegisterValue(HSQUIRRELVM vm, const char *name, bool value)
{
	g_pApi->pushconsttable(vm);
	g_pApi->pushstring(vm, name, -1);
	g_pApi->pushbool(vm, value);
	g_pApi->newslot(vm, -3, SQFalse);
	g_pApi->pop(vm, 1);
}

void SqRegisterValue(HSQUIRRELVM vm, const char *name, float value)
{
	g_pApi->pushconsttable(vm);
	g_pApi->pushstring(vm, name, -1);
	g_pApi->pushfloat(vm, value);
	g_pApi->newslot(vm, -3, SQFalse);
	g_pApi->pop(vm, 1);
}

void SqRegisterValue(HSQUIRRELVM vm, const char *name, char *value)
{
	g_pApi->pushconsttable(vm);
	g_pApi->pushstring(vm, name, -1);
	g_pApi->pushstring(vm, value, -1);
	g_pApi->newslot(vm, -3, SQFalse);
	g_pApi->pop(vm, 1);
}

#ifdef __cplusplus
extern "C" {
#endif
SQRESULT EXPORT sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	int device = -1;
	if (!BASS_Init(device, 44100, 0, 0, NULL))
	{
		std::cout << "Could not initialize sound device!" << std::endl;
		return SQ_ERROR;
	}

	std::cout << "Music module for G2O v1.0 by Nubzior" << std::endl;

	g_pApi = api;

	SqRegisterFunction(vm, "BASS_StreamCreateFile", sq_bass_stream_create_file);
	SqRegisterFunction(vm, "BASS_StreamCreateURL", sq_bass_stream_create_url);
	SqRegisterFunction(vm, "BASS_StreamFree", sq_bass_stream_free);
	SqRegisterFunction(vm, "BASS_GetVolume", sq_bass_get_volume);
	SqRegisterFunction(vm, "BASS_SetVolume", sq_bass_set_volume);
	SqRegisterFunction(vm, "BASS_ChannelStop", sq_bass_channel_stop);
	SqRegisterFunction(vm, "BASS_ChannelPlay", sq_bass_channel_play);
	SqRegisterFunction(vm, "BASS_ChannelPause", sq_bass_channel_pause);
	SqRegisterFunction(vm, "BASS_ChannelGetTags", sq_bass_channel_get_tags);

	SqRegisterValue(vm, "BASS_TAG_ID3", BASS_TAG_ID3);
	SqRegisterValue(vm, "BASS_TAG_ID3V2", BASS_TAG_ID3V2);
	SqRegisterValue(vm, "BASS_TAG_OGG", BASS_TAG_OGG);
	SqRegisterValue(vm, "BASS_TAG_HTTP", BASS_TAG_HTTP);
	SqRegisterValue(vm, "BASS_TAG_ICY", BASS_TAG_ICY);
	SqRegisterValue(vm, "BASS_TAG_META", BASS_TAG_META);
	SqRegisterValue(vm, "BASS_TAG_APE", BASS_TAG_APE);
	SqRegisterValue(vm, "BASS_TAG_MP4", BASS_TAG_MP4);
	SqRegisterValue(vm, "BASS_TAG_VENDOR", BASS_TAG_VENDOR);
	SqRegisterValue(vm, "BASS_TAG_LYRICS3", BASS_TAG_LYRICS3);
	SqRegisterValue(vm, "BASS_TAG_CA_CODEC", BASS_TAG_CA_CODEC);
	SqRegisterValue(vm, "BASS_TAG_MF", BASS_TAG_MF);
	SqRegisterValue(vm, "BASS_TAG_WAVEFORMAT", BASS_TAG_WAVEFORMAT);
	SqRegisterValue(vm, "BASS_TAG_RIFF_INFO", BASS_TAG_RIFF_INFO);
	SqRegisterValue(vm, "BASS_TAG_RIFF_BEXT", BASS_TAG_RIFF_BEXT);
	SqRegisterValue(vm, "BASS_TAG_RIFF_CART", BASS_TAG_RIFF_CART);
	SqRegisterValue(vm, "BASS_TAG_RIFF_DISP", BASS_TAG_RIFF_DISP);
	SqRegisterValue(vm, "BASS_TAG_APE_BINARY", BASS_TAG_APE_BINARY);
	SqRegisterValue(vm, "BASS_TAG_MUSIC_NAME", BASS_TAG_MUSIC_NAME);
	SqRegisterValue(vm, "BASS_TAG_MUSIC_MESSAGE", BASS_TAG_MUSIC_MESSAGE);
	SqRegisterValue(vm, "BASS_TAG_MUSIC_ORDERS", BASS_TAG_MUSIC_ORDERS);
	SqRegisterValue(vm, "BASS_TAG_MUSIC_INST", BASS_TAG_MUSIC_INST);
	SqRegisterValue(vm, "BASS_TAG_MUSIC_SAMPLE", BASS_TAG_MUSIC_SAMPLE);

	return SQ_OK;
}
#ifdef __cplusplus
} /*extern "C"*/
#endif