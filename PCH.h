#pragma once

#ifdef WIN32
#include <windows.h>
#endif

#include <stdlib.h>
#include <iostream>

#include "Squirrel/sqmodule.h"
#include "Squirrel/SqApi.h"
#include "include/bass.h"

extern HSQAPI g_pApi;

#include "SqFunction.h"