align=center][img]http://www.un4seen.com/_/logo_bass.gif[/img][/align]
 
[b]How to load module:
[/b]
Add to config
[code]
<module src="music.dll" type="client" />
[/code]
 
[b]Functions:[/b]
[color=#3366cc]userpointer[/color] handler = BASS_StreamCreateFile([color=#3366cc]string[/color] file, [color=#3366cc]int[/color] start, [color=#3366cc]int[/color] end);
[color=#3366cc]userpointer[/color] handler = BASS_StreamCreateURL([color=#3366cc]string[/color] url);
[color=#3366cc]bool[/color] BASS_StreamFree([color=#3366cc]userpointer[/color] handler);
[color=#3366cc]int[/color] BASS_GetVolume([color=#3366cc]userpointer[/color] handler);
[color=#3366cc]bool[/color] BASS_SetVolume([color=#3366cc]userpointer[/color] handler, [color=#3366cc]int[/color] volume [0 - 100]);
[color=#3366cc]bool[/color] BASS_ChannelStop([color=#3366cc]userpointer[/color] handler);
[color=#3366cc]bool[/color] BASS_ChannelPlay([color=#3366cc]userpointer[/color] handler, [color=#3366cc]bool[/color] restart);
[color=#3366cc]bool[/color] BASS_ChannelPause([color=#3366cc]userpointer[/color] handler);
[color=#3366cc]string[/color] BASS_ChannelGetTags([color=#3366cc]userpointer[/color] handler, [color=#3366cc]int[/color] tags);
 
[b]Consts:[/b]
BASS_TAG_ID3 [color=#3366cc]0[/color][color=#66cc66] // ID3v1 tags : TAG_ID3 structure[/color]
BASS_TAG_ID3V2 [color=#3366cc]1[/color][color=#66cc66] // ID3v2 tags : variable length block[/color]
BASS_TAG_OGG [color=#3366cc]2[/color][color=#66cc66] // OGG comments : series of null-terminated UTF-8 strings[/color]
BASS_TAG_HTTP [color=#3366cc]3[/color][color=#66cc66] // HTTP headers : series of null-terminated ANSI strings[/color]
BASS_TAG_ICY [color=#3366cc]4[/color][color=#66cc66] // ICY headers : series of null-terminated ANSI strings[/color]
BASS_TAG_META [color=#3366cc]5[/color][color=#66cc66] // ICY metadata : ANSI string[/color]
BASS_TAG_APE [color=#3366cc]6[/color][color=#66cc66] // APE tags : series of null-terminated UTF-8 strings[/color]
BASS_TAG_MP4 [color=#3366cc]7[/color][color=#66cc66] // MP4/iTunes metadata : series of null-terminated UTF-8 strings[/color]
BASS_TAG_VENDOR [color=#3366cc]9[/color][color=#66cc66] // OGG encoder : UTF-8 string[/color]
BASS_TAG_LYRICS3 [color=#3366cc]10[/color][color=#66cc66] // Lyric3v2 tag : ASCII string[/color]
BASS_TAG_CA_CODEC [color=#3366cc]11[/color][color=#66cc66] // CoreAudio codec info : TAG_CA_CODEC structure[/color]
BASS_TAG_MF [color=#3366cc]13[/color][color=#66cc66] // Media Foundation tags : series of null-terminated UTF-8 strings[/color]
BASS_TAG_WAVEFORMAT [color=#3366cc]14[/color][color=#66cc66] // WAVE format : WAVEFORMATEEX structure[/color]
BASS_TAG_RIFF_INFO [color=#3366cc]0x100[/color][color=#66cc66] // RIFF "INFO" tags : series of null-terminated ANSI strings[/color]
BASS_TAG_RIFF_BEXT [color=#3366cc]0x101[/color][color=#66cc66] // RIFF/BWF "bext" tags : TAG_BEXT structure[/color]
BASS_TAG_RIFF_CART [color=#3366cc]0x102[/color][color=#66cc66] // RIFF/BWF "cart" tags : TAG_CART structure[/color]
BASS_TAG_RIFF_DISP [color=#3366cc]0x103[/color][color=#66cc66] // RIFF "DISP" text tag : ANSI string[/color]
BASS_TAG_APE_BINARY [color=#3366cc]0x1000[/color][color=#66cc66] // + index #, binary APE tag : TAG_APE_BINARY structure[/color]
BASS_TAG_MUSIC_NAME [color=#3366cc]0x10000[/color][color=#66cc66] // MOD music name : ANSI string[/color]
BASS_TAG_MUSIC_MESSAGE [color=#3366cc]0x10001[/color][color=#66cc66] // MOD message : ANSI string[/color]
BASS_TAG_MUSIC_ORDERS [color=#3366cc]0x10002[/color][color=#66cc66] // MOD order list : BYTE array of pattern numbers[/color]
BASS_TAG_MUSIC_INST [color=#3366cc]0x10100[/color][color=#66cc66] // + instrument #, MOD instrument name : ANSI string[/color]
BASS_TAG_MUSIC_SAMPLE [color=#3366cc]0x10300[/color][color=#66cc66] // + sample #, MOD sample name : ANSI string[/color]
 
[b]1.0 Download:[/b]
Binares: [url=https://bitbucket.org/Nubzior/music-module-for-g2o/downloads/]https://bitbucket.org/Nubzior/music-module-for-g2o/downloads/[/url]
Source code: [url=https://bitbucket.org/Nubzior/music-module-for-g2o/src/]https://bitbucket.org/Nubzior/music-module-for-g2o/src/[/url]