#include "PCH.h"

SQFUNC(sq_bass_stream_create_file)
{
	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 3)
		return g_pApi->throwerror(vm, "(BASS_StreamCreateFile) wrong number of parameters, expecting 3");

	if (g_pApi->gettype(vm, 2) != OT_STRING)
		return g_pApi->throwerror(vm, "(BASS_StreamCreateFile) wrong type of parameter 0, expecting 'string'");

	if (g_pApi->gettype(vm, 3) != OT_INTEGER)
		return g_pApi->throwerror(vm, "(BASS_StreamCreateFile) wrong type of parameter 1, expecting 'int'");

	if (g_pApi->gettype(vm, 4) != OT_INTEGER)
		return g_pApi->throwerror(vm, "(BASS_StreamCreateFile) wrong type of parameter 2, expecting 'int'");

	const SQChar *sFile;
	SQInteger iStart;
	SQInteger iEnd;

	g_pApi->getstring(vm, 2, &sFile);
	g_pApi->getinteger(vm, 3, &iStart);
	g_pApi->getinteger(vm, 4, &iEnd);

	SQInteger stream = BASS_StreamCreateFile(false, sFile, iStart, iEnd, 0);
	g_pApi->pushinteger(vm, stream);

	return 1;
}

SQFUNC(sq_bass_stream_create_url)
{
	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 1)
		return g_pApi->throwerror(vm, "(BASS_StreamCreateURL) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, 2) != OT_STRING)
		return g_pApi->throwerror(vm, "(BASS_StreamCreateURL) wrong type of parameter 0, expecting 'string'");

	const SQChar *sURL;

	g_pApi->getstring(vm, 2, &sURL);

	SQInteger stream = BASS_StreamCreateURL(sURL, 0, 0, 0, 0);
	g_pApi->pushinteger(vm, stream);
}

SQFUNC(sq_bass_stream_free)
{
	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 1)
		return g_pApi->throwerror(vm, "(BASS_StreamFree) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, 2) != OT_INTEGER)
		return g_pApi->throwerror(vm, "(BASS_StreamFree) wrong type of parameter 0, expecting 'int'");

	SQInteger iStream;

	g_pApi->getinteger(vm, 2, &iStream);

	if (BASS_StreamFree(iStream))
	{
		g_pApi->pushbool(vm, true);
	}
	else
	{
		g_pApi->pushbool(vm, false);
	}

	return 1;
}

SQFUNC(sq_bass_get_volume)
{
	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 1)
		return g_pApi->throwerror(vm, "(BASS_GetVolume) wrong number of parameters, expecting 1");

	if (g_pApi->gettype(vm, 2) != OT_INTEGER)
		return g_pApi->throwerror(vm, "(BASS_GetVolume) wrong type of parameter 0, expecting 'int'");

	SQInteger iStream;

	g_pApi->getinteger(vm, 2, &iStream);

	float fVol = 0.f;
	BASS_ChannelGetAttribute(iStream, BASS_ATTRIB_VOL, &fVol);

	SQFloat rVol = (float)fVol;
	rVol *= 100;
	g_pApi->pushinteger(vm, (int)rVol);

	return 1;
}

SQFUNC(sq_bass_set_volume)
{
	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 2)
		return g_pApi->throwerror(vm, "(BASS_SetVolume) wrong number of parameters, expecting 2");

	if (g_pApi->gettype(vm, 2) != OT_INTEGER)
		return g_pApi->throwerror(vm, "(BASS_SetVolume) wrong type of parameter 0, expecting 'int'");

	if (g_pApi->gettype(vm, 3) != OT_INTEGER)
		return g_pApi->throwerror(vm, "(BASS_SetVolume) wrong type of parameter 1, expecting 'int'");

	SQInteger iStream;
	SQInteger iVol;

	g_pApi->getinteger(vm, 2, &iStream);
	g_pApi->getinteger(vm, 3, &iVol);

	float fVolume = (float(iVol) / 100);

	if (BASS_ChannelSetAttribute(iStream, BASS_ATTRIB_VOL, fVolume))
	{
		g_pApi->pushbool(vm, true);
	}
	else
	{
		g_pApi->pushbool(vm, false);
	}

	return 1;
}

SQFUNC(sq_bass_channel_stop)
{
	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 1)
		return g_pApi->throwerror(vm, "(BASS_ChannelStop) wrong number of parameters, expecting 2");

	if (g_pApi->gettype(vm, 2) != OT_INTEGER)
		return g_pApi->throwerror(vm, "(BASS_ChannelStop) wrong type of parameter 0, expecting 'int'");

	SQInteger iStream;

	g_pApi->getinteger(vm, 2, &iStream);

	if (BASS_ChannelStop(iStream))
	{
		g_pApi->pushbool(vm, true);
	}
	else
	{
		g_pApi->pushbool(vm, false);
	}

	return 1;
}

SQFUNC(sq_bass_channel_play)
{
	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 2)
		return g_pApi->throwerror(vm, "(BASS_ChannelPlay) wrong number of parameters, expecting 2");

	if (g_pApi->gettype(vm, 2) != OT_INTEGER)
		return g_pApi->throwerror(vm, "(BASS_ChannelPlay) wrong type of parameter 0, expecting 'int'");

	if (g_pApi->gettype(vm, 3) != OT_BOOL)
		return g_pApi->throwerror(vm, "(BASS_ChannelPlay) wrong type of parameter 1, expecting 'bool'");

	SQInteger iStream;
	SQBool bRestart;

	g_pApi->getinteger(vm, 2, &iStream);
	g_pApi->getbool(vm, 3, &bRestart);

	if (BASS_ChannelPlay(iStream, bRestart))
	{
		g_pApi->pushbool(vm, true);
	}
	else
	{
		g_pApi->pushbool(vm, false);
	}

	return 1;
}

SQFUNC(sq_bass_channel_pause)
{
	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 1)
		return g_pApi->throwerror(vm, "(BASS_ChannelPause) wrong number of parameters, expecting 2");

	if (g_pApi->gettype(vm, 2) != OT_INTEGER)
		return g_pApi->throwerror(vm, "(BASS_ChannelPause) wrong type of parameter 0, expecting 'int'");

	SQInteger iStream;

	g_pApi->getinteger(vm, 2, &iStream);

	if (BASS_ChannelPause(iStream))
	{
		g_pApi->pushbool(vm, true);
	}
	else
	{
		g_pApi->pushbool(vm, false);
	}

	return 1;
}

SQFUNC(sq_bass_channel_get_tags)
{
	int iArgs = g_pApi->gettop(vm) - 1;

	if (iArgs != 2)
		return g_pApi->throwerror(vm, "(BASS_ChannelGetTags) wrong number of parameters, expecting 2");

	if (g_pApi->gettype(vm, 2) != OT_INTEGER)
		return g_pApi->throwerror(vm, "(BASS_ChannelGetTags) wrong type of parameter 0, expecting 'int'");

	if (g_pApi->gettype(vm, 3) != OT_BOOL)
		return g_pApi->throwerror(vm, "(BASS_ChannelGetTags) wrong type of parameter 1, expecting 'int'");

	SQInteger iStream;
	SQInteger iTag;

	g_pApi->getinteger(vm, 2, &iStream);
	g_pApi->getinteger(vm, 3, &iTag);

	const SQChar *tagRet = BASS_ChannelGetTags(iStream, iTag);
	g_pApi->pushstring(vm, tagRet, -1);

	return 1;
}